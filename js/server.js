/*eslint no-console: 0*/
"use strict";

var request = require("request");

var http = require("http");
var port = process.env.PORT || 3000;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

http.createServer(function(req, res) {
	var svcs = JSON.parse(process.env.VCAP_SERVICES);
	var creds = svcs.jobscheduler[0].credentials;
	var pass = creds.password;
	var url = creds.url;
	var user = creds.user;

	var auth = "Basic " + new Buffer(user + ":" + pass).toString("base64");

	console.log("Jobs URL = " + url + "");
	// console.log("Authorization : \"" + auth + "\"\n");

	var app = JSON.parse(process.env.VCAP_APPLICATION);
	var uri = app.full_application_uris[0];

	if ((req.method == "GET") && (req.url == "/jobs.js")) { // List Jobs

		request({
			url: url + "/scheduler/jobs",
			method: "GET",
			headers: {
				"Authorization": auth
			}
		}, function(error, response, body) {
			if (response.statusCode == 200) {
				//console.log("Body:\n" + body + "\n");
				var jb = JSON.parse(body);
				if (jb.results.length > 0) {
					var num = jb.results[0].jobId;
					var nam = jb.results[0].name;
					console.log("Job# " + num + " " + nam + "\n");
				} else {
					console.log("No Jobs");
				}
			} else {
				console.log(error);
			}

		});
		res.writeHead(200, {
			"Content-Type": "application/json"
		});
		res.end("List Jobs\n\nLook at Logs produced by the NodeJS module server.js\nNotice list of currently running jobs.");
	} else if ((req.method == "GET") && (req.url == "/start.js")) { // Start = Schedule
		var myJSONObject = {
			"name": "validateSalesOrder",
			"description": "cron job that validates sales order requests",
			"action": uri + "/hello.js",
			"active": true,
			"httpMethod": "GET",
			"schedules": [{
				"cron": "* * * * * * */10",
				"description": "this schedule runs every 10 seconds",
				"data": {
					"salesOrderId": "1234"
				},
				"active": true,
				"startTime": {
					"date": "2015-10-20 04:30 +0000",
					"format": "YYYY-MM-DD HH:mm Z"
				}
			}]
		};

		request({
			url: url + "/scheduler/jobs",
			method: "POST",
			headers: {
				"Authorization": auth
			},
			json: true, // <--Very important!!!
			body: myJSONObject
		}, function(error, response, body) {
			console.log(response);
			if (response.statusCode == 201) {
				console.log(body);
			} else {
				console.log("Error:" + error);
			}

		});

		res.writeHead(200, {
			"Content-Type": "text/plain"
		});
		res.end("Start\n\nLook at Logs produced by the NodeJS module server.js\nNotice additional Job URL lines appear every 10 seconds (refresh page).");

	} else if ((req.method == "GET") && (req.url == "/stop.js")) { // Stop = Unschedule first found
		// http://help.sap.com/saphelp_hanaplatform/helpdata/en/79/1a171998384f029ae544f0b266cecc/content.htm?frameset=/en/e9/f5128e8acb4efe8faa0502e448c27f/frameset.htm&current_toc=/en/ea/d6bf24e92745949a91b9ff2216af50/plain.htm&node_id=175&show_children=false
		/*
DELETE /scheduler/jobs/4 HTTP/1.1
Host: localhost:4242
Authorization: Basic YWJjOmRlZg==
Content-Type: application/json
Cache-Control: no-cache	
	*/

		request({
			url: url + "/scheduler/jobs",
			method: "GET",
			headers: {
				"Authorization": auth
			}
		}, function(error, response, body) {
			if (response.statusCode == 200) {
				//console.log("Body:\n" + body + "\n");
				var jb = JSON.parse(body);
				if (jb.results.length > 0) {
					var num = jb.results[0].jobId;
					var nam = jb.results[0].name;
					console.log("Job# " + num + " " + nam);
					request({
						url: url + "/scheduler/jobs/" + num,
						method: "DELETE",
						headers: {
							"Authorization": auth
						}
					}, function(error, response, body) {
						if (response.statusCode == 200) {
							console.log(body);
						} else {
							console.log(error);
						}

					});
				} else {
					console.log("No Jobs");
				}
			} else {
				console.log(error);
			}

		});

		res.writeHead(200, {
			"Content-Type": "text/plain"
		});
		res.end("Stop\n\nLook at Logs produced by the NodeJS module server.js\nNotice Job# N validateSalesOrder at the end and no additional Jobs URL lines.");
	} else if ((req.method == "GET") && (req.url == "/hello.js")) { // Hello = Something to call repeatedly
		res.writeHead(200, {
			"Content-Type": "text/plain"
		});
		res.end("Hello World\n\nLook at Logs produced by the NodeJS module server.js (refresh page).");
	} else { // Default
		res.writeHead(200, {
			"Content-Type": "text/plain"
		});
		res.end("Hello World\n");
	}
}).listen(port);

console.log("Server listening on port %d", port);